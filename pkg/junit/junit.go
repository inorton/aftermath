package junit

import "encoding/xml"

type TestSuites struct {
	// a totally normal junit report
	XMLName  xml.Name    `xml:"testsuites"`
	Duration string      `xml:"duration,attr"`
	Suites   []TestSuite `xml:"testsuite"`
}

type TestRun struct {
	// apparently this comes out of eclipse
	// very tempted to not support this
	// these can contain a tree of <testsuite> elements!
	XMLName xml.Name        `xml:"testrun"`
	Suites  []TestSuiteTree `xml:"testsuite"`
	Name    string          `xml:"name,attr"`
	Project string          `xml:"project,attr"`
}

type TestSuite struct {
	XMLName xml.Name `xml:"testsuite"`

	Hostname  string `xml:"hostname,attr"`
	Name      string `xml:"name,attr"`
	Package   string `xml:"package,attr"`
	Time      string `xml:"time,attr"`
	Timestamp string `xml:"timestamp,attr"`

	Properties []Property `xml:"properties>property"`
	StdOut     string     `xml:"system-out"`
	StdErr     string     `xml:"system-err"`
	Error      Error      `xml:"error"`

	Tests []TestCase `xml:"testcase"`
}

type TestSuiteTree struct {
	TestSuite

	// some crazy reports can contain a nested <testsuite> (see TestRun)
	Children []TestSuite `xml:"testsuite"`
}

type Outcome struct {
	Name    string `xml:"name,attr"`
	Message string `xml:"message,attr"`
	Type    string `xml:"type,attr"`
	Content string `xml:",chardata"`
}

type Skipped struct {
	Outcome
	XMLName xml.Name `xml:"skipped"`
}

type Failure struct {
	Outcome
	XMLName xml.Name `xml:"failure"`
}

type Error struct {
	Outcome
	XMLName xml.Name `xml:"error"`
}

type TestCase struct {
	XMLName xml.Name `xml:"testcase"`
	Name    string   `xml:"name,attr"`
	Class   string   `xml:"classname,attr"`
	Status  string   `xml:"status,attr"`
	Time    string   `xml:"time"`
	Skipped Skipped  `xml:"skipped"`
	Error   Error    `xml:"error"`
	Failure Failure  `xml:"failure"`
	StdOut  string   `xml:"system-out"`
	StdErr  string   `xml:"system-err"`
}

type Property struct {
	XMLName xml.Name `xml:"property"`
	Name    string   `xml:"name,attr"`
	Value   string   `xml:"value,attr"`
}

type Report struct {
	TestSuites []TestSuites
}

func (j *Report) Parse(input []byte) error {

	// assume a totally normal report rooted at <testsuites>
	j.TestSuites = []TestSuites{}

	err := xml.Unmarshal(input, &j.TestSuites)

	// note, some reports are rooted at "testsuite"
	if err != nil {
		suite := TestSuite{}
		// don't leak this error out
		if xml.Unmarshal(input, &suite) == nil {
			suites := TestSuites{}
			suites.Suites = append(suites.Suites, suite)
			j.TestSuites = append(j.TestSuites, suites)
			err = nil
		}
	}

	// could be rooted at <testrun> tend to have NESTED <testsuite>s

	return err
}

func (o *Outcome) IsTrue() bool {
	return o.Name != "" || o.Message != "" || o.Type != "" || o.Content != ""
}

func (t *TestCase) IsFailed() bool {
	return t.Failure.IsTrue()
}

func (t *TestCase) IsSkipped() bool {
	return t.Skipped.IsTrue()
}

func (t *TestCase) IsError() bool {
	return t.Error.IsTrue()
}

func (t *TestCase) IsPass() bool {
	return !(t.IsFailed() && t.IsError() && t.IsSkipped())
}
