package models

import (
	"github.com/jinzhu/gorm"
)

type Project struct {
	gorm.Model
	ParentID uint
	Name     string
	Projects []Project `gorm:"foreign_key:ParentID"`
	Runs     []Run
}
