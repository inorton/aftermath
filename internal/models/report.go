package models

import "github.com/jinzhu/gorm"

type Report struct {
	gorm.Model
	Name     string
	RunID    uint
	Outcome  string
	Comments []ReportComment
	Raw      string
}
