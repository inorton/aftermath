package models

import "github.com/jinzhu/gorm"

type Run struct {
	gorm.Model
	ProjectID uint
	Name      string
	Reports   []Report
	Comments  []RunComment
}
