package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type TestSuite struct {
	gorm.Model
	XName       string
	XID         string
	XPackage    string
	XTime       time.Duration
	XTimestamp  time.Time
	XHostname   string
	XDisabled   bool
	XSkipped    bool
	XProperties []Property
	XSysOut     string
	XSysErr     string
	XTestCases  []TestCase
}

type Property struct {
	gorm.Model
	TestSuiteID uint
	Key         string
	Value       string
}

type TestCase struct {
	gorm.Model
	TestSuiteID uint
	XName       string
	XID         string
	XTime       time.Duration
}
