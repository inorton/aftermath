package models

import "github.com/jinzhu/gorm"

type Comment struct {
	gorm.Model
	From    string
	Comment string
}

type ReportComment struct {
	Comment
	ReportID uint
}

type RunComment struct {
	Comment
	RunID uint
}
