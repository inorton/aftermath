package junit

// test that we can parse most of the same things that junit2html can

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/inorton/aftermath/pkg/junit"
	"gitlab.com/inorton/aftermath/test"
	"testing"
)

// assert values are correct for junit-simple_suite*.xml reports
func assert_simple_suites(t *testing.T, report *junit.Report) {
	assert.Equal(t, 1, len(report.TestSuites), "expected 1 testsuite")
	suites := report.TestSuites[0].Suites
	assert.Equal(t, 1, len(suites))
	suite := suites[0]
	assert.Equal(t, "JUnitXmlReporter.constructor", suite.Name)
	assert.Equal(t, "0.006", suite.Time)
	assert.Equal(t, "2013-05-24T10:23:58", suite.Timestamp)
	props := suite.Properties
	assert.Equal(t, 3, len(props))
	assert.Equal(t, "java.vendor", props[0].Name)
	assert.Equal(t, "Sun Microsystems Inc.", props[0].Value)
	assert.Equal(t, "compiler.debug", props[1].Name)
	assert.Equal(t, "on", props[1].Value)
	assert.Equal(t, "project.jdk.classpath", props[2].Name)
	assert.Equal(t, "jdk.classpath.1.6", props[2].Value)
	tests := suite.Tests
	assert.Equal(t, 1, len(tests))
	assert.Equal(t, "should default path to an empty string", tests[0].Name)
	assert.Equal(t, "JUnitXmlReporter.constructor", tests[0].Class)
	assert.False(t, tests[0].IsError())
	assert.False(t, tests[0].IsSkipped())
	assert.True(t, tests[0].IsFailed())
	assert.Equal(t, "test failure", tests[0].Failure.Message)
	assert.Equal(t, "Assertion failed", tests[0].Failure.Content)
	assert.Equal(t, "", tests[0].Failure.Type)
}

func TestParseJUnitXML_simple_suites(t *testing.T) {
	bytes := tests.GetDataFileBytes("junit-simple_suites.xml")
	report := junit.Report{}
	err := report.Parse(bytes)
	assert.NoError(t, err, "failed to parse report")

	assert_simple_suites(t, &report)
}

func TestParseJUnitXML_simple_suite(t *testing.T) {
	bytes := tests.GetDataFileBytes("junit-simple_suite.xml")
	report := junit.Report{}
	err := report.Parse(bytes)
	assert.NoError(t, err, "failed to parse report")

	assert_simple_suites(t, &report)
}

func TestParseJUnitXML_cute2(t *testing.T) {
	bytes := tests.GetDataFileBytes("junit-cute2.xml")
	report := junit.Report{}
	err := report.Parse(bytes)
	assert.NoError(t, err, "failed to parse report")

	assert.Equal(t, 1, len(report.TestSuites))

	suites := report.TestSuites[0].Suites
	assert.Equal(t, 6, len(suites))

	assert.Equal(t, "All", suites[0].Name)
	assert.Equal(t, "Platform", suites[1].Name)
	assert.Equal(t, "IpNet", suites[2].Name)
	assert.Equal(t, "LinuxIp", suites[3].Name)
	assert.Equal(t, "String", suites[4].Name)
	assert.Equal(t, "MultiOs", suites[5].Name)

	linuxIp := suites[3]
	assert.Equal(t, 10, len(linuxIp.Tests))

	multiOs := suites[5]
	setMinusOne := multiOs.Tests[17]
	assert.True(t, setMinusOne.IsFailed())
	assert.Equal(t, "MultiOS/Message.cpp:536 \"Set value to -1 should throw\"", setMinusOne.Failure.Message)
	assert.Equal(t, "\n\"Set value to -1 should throw\"\n\t\t\t", setMinusOne.Failure.Content)
}

func TestParseJUnitXML_unicode(t *testing.T) {
	bytes := tests.GetDataFileBytes("junit-unicode.xml")
	report := junit.Report{}
	err := report.Parse(bytes)
	assert.NoError(t, err, "failed to parse report")

	assert.Equal(t, 1, len(report.TestSuites), "expected 1 testsuite")
	suites := report.TestSuites[0].Suites
	assert.Equal(t, 1, len(suites))
	suite := suites[0]

	assert.True(t, suite.Tests[0].IsFailed())
	assert.Equal(t,
		"Expected <anonymous>({ 3: 1, Hi: 1, hi: 1, constructor: 1, _: 1, 10€: 2 }) to equal <anonymous>({ 3: 1, hi: 2, constructor: 1, _: 1, 10€: 2 }).\n/home/gereon/Schreibtisch/evalTest/ex-7_9-moodle-karma-test-execution/test-classes/test_ex4.js:40:50\n    ",
		suite.Tests[0].Failure.Content)
}

func TestParseJUnitXML_unicode2(t *testing.T) {
	bytes := tests.GetDataFileBytes("junit-unicode2.xml")
	report := junit.Report{}
	err := report.Parse(bytes)
	assert.NoError(t, err, "failed to parse report")

	assert.Equal(t, 1, len(report.TestSuites), "expected 1 testsuite")
	suites := report.TestSuites[0].Suites
	assert.Equal(t, 1, len(suites))
	suite := suites[0]

	assert.Equal(t, "Дратути", suite.Name)
	assert.True(t, suite.Tests[0].IsPass())
	assert.Equal(t, "Этот кейс упадёт", suite.Tests[0].Name)
	assert.Equal(t, "Дратути", suite.Tests[0].Class)
}
