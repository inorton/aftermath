package tests

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
)

func GetDataPath() string {
	_, filename, _, _ := runtime.Caller(0)
	return filepath.Join(filepath.Dir(filename), "data")
}

func GetDataFilePath(filename string) string {
	return filepath.Join(GetDataPath(), filename)
}

func GetDataFileBytes(filename string) []byte {
	path := GetDataFilePath(filename)

	f, err := os.Open(path)

	if err != nil {
		log.Panic("can't open file " + path)
	}

	defer f.Close()

	bytes, err := ioutil.ReadAll(f)
	if err != nil {
		log.Panic("can't read file " + path)
	}
	return bytes
}
